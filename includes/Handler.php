<?php
/**
 * Copyright (C) 2022  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Mark A. Hershberger <mah@everybody.org>
 */

namespace MediaWiki\Extension\HTTPAuth;

use ConfigFactory;
use MediaWiki\Extension\PluggableAuth\PluggableAuth;
use MediaWiki\User\UserIdentity;
use PasswordError;
use PasswordFactory;
use User;
use Wikimedia\Rdbms\ILoadBalancer;

class Handler extends PluggableAuth {

	private Config $config;
	private ILoadBalancer $dbload;
	private PasswordFactory $pwf;

	/**
	 * Load the services
	 *
	 * @param ConfigFactory $cf
	 * @param ILoadBalancer $dbload
	 * @param PasswordFactory $pwf
	 */
	public function __construct(
		ConfigFactory $cf,
		ILoadBalancer $dbload,
		PasswordFactory $pwf
	) {
		$this->config = $cf->makeConfig( "HTTPAuth" );
		$this->dbload = $dbload;
		$this->pwf = $pwf;
	}

	/**
	 * @param int|null &$id The user's user ID
	 * @param string|null &$username The user's username
	 * @param string|null &$realname The user's real name
	 * @param string|null &$email The user's email address
	 * @param string|null &$errorMessage Returns a descriptive message if there's an error
	 * @return bool true if the user has been authenticated and false otherwise
	 * @since 1.0
	 *
	 */
	public function authenticate(
		?int &$id,
		?string &$username,
		?string &$realname,
		?string &$email,
		?string &$errorMessage
	): bool {
		$username = $_SERVER['PHP_AUTH_USER'] ?? null;
		$pass = $_SERVER['PHP_AUTH_PW'] ?? null;

		try {
			$user = $username !== null ? User::newFromName( $username ) : false;

			if ( $username !== null && $pass !== null && $user !== false ) {
				$status = $user->checkPasswordValidity( $username, $pass );
				if ( !$status->isOK() ) {
					wfDebugLog( "authentication", "Invalid password for '$username'" );
					// Fatal, can't log in
					throw new Exception( $status->getMessage() );
				}
				wfDebugLog( "authentication", "Checking '$username'" );
				$response = $this->checkPassword( $username, $pass );
				$realname = $user->getRealname();
				$email = $user->getEmail();

				return true;
			}
		} catch ( Exception $e ) {
			wfDebugLog( "authentication", "Exception for '$username'" );
			$this->logger->debug( $e->__toString() . PHP_EOL );
		}
		header(
			sprintf( 'WWW-Authenticate: Basic realm="%s"', $this->config->get( Config::REALM ) )
		);
		header( 'HTTP/1.0 401 Unauthorized' );
		echo wfMessage( "http-auth-auth-needed" )->plain();
		exit;
	}

	/**
	 * @fixme This function is basically lifted from LocalPasswordPrimaryAuthenticationProvider
	 *
	 * @param string $username user's username
	 * @param string $pass plain text password from user
	 * @return bool
	 * @throws Exception
	 */
	protected function checkPassword( string $username, string $pass ): bool {
		$fields = [
			'user_id', 'user_password', 'user_password_expires',
		];

		$dbr = $this->dbload->getConnectionRef( DB_REPLICA );
		$row = $dbr->selectRow(
			'user',
			$fields,
			[ 'user_name' => $username ],
			__METHOD__
		);
		if ( !$row ) {
			wfDebugLog( "authentication", "No matching user for '$username'" );
			throw new Exception( 'wrongpassword' );
		}

		// Check for *really* old password hashes that don't even have a type
		// The old hash format was just an md5 hex hash, with no type information
		if ( preg_match( '/^[0-9a-f]{32}$/', $row->user_password ) ) {
			$row->user_password = ":B:{$row->user_id}:{$row->user_password}";
		}

		$pwhash = $this->getPassword( $row->user_password );
		if ( !$pwhash->verify( $pass ) ) {
			wfDebugLog( "authentication", "Password does not match for '$username'" );
			throw new Exception( 'wrongpassword' );
		}

		return true;
	}

	/**
	 * Get a Password object from the hash
	 * @param string $hash
	 * @return Password
	 */
	protected function getPassword( $hash ) {
		try {
			return $this->pwf->newFromCiphertext( $hash );
		} catch ( PasswordError $e ) {
			$class = static::class;
			$this->logger->debug( "Invalid password hash in {$class}::getPassword()" );
			return $this->pwf->newFromCiphertext( null );
		}
	}

	/**
	 * @inheritDoc
	 */
	public function saveExtraAttributes( int $id ): void {
	}

	/**
	 * @inheritDoc
	 */
	public function deauthenticate( UserIdentity &$user ): void {
	}
}
